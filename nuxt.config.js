export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'rostomyan-artem',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: "stylesheet", href: "https://unpkg.com/aos@2.3.1/dist/aos.css"}
    ]
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    `~/assets/scss/style.scss`,
    "aos/dist/aos.css"
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src: "@/plugins/aos", ssr: false},
    {src: "@/plugins/scroll", ssr: false}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // 'vue-scrollto/nuxt',

    //     // Or if you have custom options...
    // ['vue-scrollto/nuxt', { duration: 300 }],
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    vendor: ["aos"]
  }
}

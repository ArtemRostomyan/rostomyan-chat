export const state = () => ({
    chats: [
        {
            "name": "Basic chat",
            "link": "basic",
            "description": "Отличный чат с полупрозрачным фоном, с подсветкой по бокам, вооружён - маленьким пистолетиком :)",
            "image": "https://i.ibb.co/cwXcBYf/basic.png",
            "sizes": [
                {
                    "name": "ширина",
                    "value": "560"
                },
                {
                    "name": "высота",
                    "value": "800"
                }
            ]
        },
        {
            "name": "Gray chat",
            "link": "gray",
            "description": "Отличный чат с полупрозрачным фоном, с подсветкой по бокам, вооружён - маленьким пистолетиком :)",
            "image": "https://i.ibb.co/60J6WSQ/gray.png",
            "sizes": [
                {
                    "name": "ширина",
                    "value": "560"
                },
                {
                    "name": "высота",
                    "value": "800"
                }
            ]
        },
        {
            "name": "Neon chat",
            "link": "neon",
            "description": "Отличный чат с полупрозрачным фоном, с подсветкой по бокам, вооружён - маленьким пистолетиком :)",
            "image": "https://i.ibb.co/QnGpbZx/neon.png",
            "sizes": [
                {
                    "name": "ширина",
                    "value": "560"
                },
                {
                    "name": "высота",
                    "value": "800"
                }
            ]
        },
        {
            "name": "Kitty chat",
            "link": "kitty",
            "dop_class": "chat-item__kitty",
            "description": "Отличный чат с полупрозрачным фоном, с подсветкой по бокам, вооружён - маленьким пистолетиком :)",
            "image": "https://i.ibb.co/rxG1hBm/kitty.png",
            "sizes": [
                {
                    "name": "ширина",
                    "value": "560"
                },
                {
                    "name": "высота",
                    "value": "800"
                }
            ]
        }
    ],
    chat: []
})

export const mutations = {
    SET_CHAT_IN_STATE(state, new_koment){
        if(state.chat.length >= 30){
            state.chat.splice(0, 20)
            state.chat.push(new_koment)
        } else{
            state.chat.push(new_koment)
        }
    }
}

export const actions = {
    // async SET_CATALOG_FROM_SERVER({commit}) {
    //     const catalog = await (await fetch('https://raw.githubusercontent.com/ArtemRostomyan/server/main/server.js')).json()
    //     commit('SET_CATALOG_FROM_SERVER', catalog)
    // }
}

export const getters = {
    CHATS: s => {
        return s.chats
    },
    CHAT : s => {
        return s.chat
    }
}
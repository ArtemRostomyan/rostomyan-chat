import { wrapFunctional } from './utils'

export { default as CatalogChatsVChatCatalog } from '../..\\components\\catalog-chats\\v-chat-catalog.vue'
export { default as CatalogChatsVOpenChat } from '../..\\components\\catalog-chats\\v-open-chat.vue'
export { default as ChatVBasicChat } from '../..\\components\\chat\\v-basic-chat.vue'
export { default as ChatVGrayChat } from '../..\\components\\chat\\v-gray-chat.vue'
export { default as ChatVKittyChat } from '../..\\components\\chat\\v-kitty-chat.vue'
export { default as ChatVNeonChat } from '../..\\components\\chat\\v-neon-chat.vue'
export { default as GlavnayaVGlavnaya } from '../..\\components\\glavnaya\\v-glavnaya.vue'
export { default as MyProductsVMyProducts } from '../..\\components\\my_products\\v-my-products.vue'

export const LazyCatalogChatsVChatCatalog = import('../..\\components\\catalog-chats\\v-chat-catalog.vue' /* webpackChunkName: "components/catalog-chats-v-chat-catalog" */).then(c => wrapFunctional(c.default || c))
export const LazyCatalogChatsVOpenChat = import('../..\\components\\catalog-chats\\v-open-chat.vue' /* webpackChunkName: "components/catalog-chats-v-open-chat" */).then(c => wrapFunctional(c.default || c))
export const LazyChatVBasicChat = import('../..\\components\\chat\\v-basic-chat.vue' /* webpackChunkName: "components/chat-v-basic-chat" */).then(c => wrapFunctional(c.default || c))
export const LazyChatVGrayChat = import('../..\\components\\chat\\v-gray-chat.vue' /* webpackChunkName: "components/chat-v-gray-chat" */).then(c => wrapFunctional(c.default || c))
export const LazyChatVKittyChat = import('../..\\components\\chat\\v-kitty-chat.vue' /* webpackChunkName: "components/chat-v-kitty-chat" */).then(c => wrapFunctional(c.default || c))
export const LazyChatVNeonChat = import('../..\\components\\chat\\v-neon-chat.vue' /* webpackChunkName: "components/chat-v-neon-chat" */).then(c => wrapFunctional(c.default || c))
export const LazyGlavnayaVGlavnaya = import('../..\\components\\glavnaya\\v-glavnaya.vue' /* webpackChunkName: "components/glavnaya-v-glavnaya" */).then(c => wrapFunctional(c.default || c))
export const LazyMyProductsVMyProducts = import('../..\\components\\my_products\\v-my-products.vue' /* webpackChunkName: "components/my-products-v-my-products" */).then(c => wrapFunctional(c.default || c))

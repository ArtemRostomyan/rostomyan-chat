# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<CatalogChatsVChatCatalog>` | `<catalog-chats-v-chat-catalog>` (components/catalog-chats/v-chat-catalog.vue)
- `<CatalogChatsVOpenChat>` | `<catalog-chats-v-open-chat>` (components/catalog-chats/v-open-chat.vue)
- `<ChatVBasicChat>` | `<chat-v-basic-chat>` (components/chat/v-basic-chat.vue)
- `<ChatVGrayChat>` | `<chat-v-gray-chat>` (components/chat/v-gray-chat.vue)
- `<ChatVKittyChat>` | `<chat-v-kitty-chat>` (components/chat/v-kitty-chat.vue)
- `<ChatVNeonChat>` | `<chat-v-neon-chat>` (components/chat/v-neon-chat.vue)
- `<GlavnayaVGlavnaya>` | `<glavnaya-v-glavnaya>` (components/glavnaya/v-glavnaya.vue)
- `<MyProductsVMyProducts>` | `<my-products-v-my-products>` (components/my_products/v-my-products.vue)

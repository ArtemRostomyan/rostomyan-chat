import Vue from 'vue'
import { wrapFunctional } from './utils'

const components = {
  CatalogChatsVChatCatalog: () => import('../..\\components\\catalog-chats\\v-chat-catalog.vue' /* webpackChunkName: "components/catalog-chats-v-chat-catalog" */).then(c => wrapFunctional(c.default || c)),
  CatalogChatsVOpenChat: () => import('../..\\components\\catalog-chats\\v-open-chat.vue' /* webpackChunkName: "components/catalog-chats-v-open-chat" */).then(c => wrapFunctional(c.default || c)),
  ChatVBasicChat: () => import('../..\\components\\chat\\v-basic-chat.vue' /* webpackChunkName: "components/chat-v-basic-chat" */).then(c => wrapFunctional(c.default || c)),
  ChatVGrayChat: () => import('../..\\components\\chat\\v-gray-chat.vue' /* webpackChunkName: "components/chat-v-gray-chat" */).then(c => wrapFunctional(c.default || c)),
  ChatVKittyChat: () => import('../..\\components\\chat\\v-kitty-chat.vue' /* webpackChunkName: "components/chat-v-kitty-chat" */).then(c => wrapFunctional(c.default || c)),
  ChatVNeonChat: () => import('../..\\components\\chat\\v-neon-chat.vue' /* webpackChunkName: "components/chat-v-neon-chat" */).then(c => wrapFunctional(c.default || c)),
  GlavnayaVGlavnaya: () => import('../..\\components\\glavnaya\\v-glavnaya.vue' /* webpackChunkName: "components/glavnaya-v-glavnaya" */).then(c => wrapFunctional(c.default || c)),
  MyProductsVMyProducts: () => import('../..\\components\\my_products\\v-my-products.vue' /* webpackChunkName: "components/my-products-v-my-products" */).then(c => wrapFunctional(c.default || c))
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}

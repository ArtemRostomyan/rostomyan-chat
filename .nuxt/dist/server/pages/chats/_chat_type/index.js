exports.ids = [10,2];
exports.modules = {

/***/ 41:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(52);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("a4f984c2", content, true, context)
};

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_open_chat_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(41);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_open_chat_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_open_chat_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_open_chat_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_open_chat_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 52:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(15);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(53);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".c-link{position:absolute;-webkit-tap-highlight-color:rgba(0,0,0,0);top:50%;left:50%;transform:translate(-50%,-50%);width:80%;padding:20px;border-radius:10px;border:2px solid #fff;display:flex;justify-content:center;transition:.3s;align-items:center;background:#ff2a76;background:linear-gradient(90deg,rgba(255,42,117,.3),rgba(0,9,107,.6));box-shadow:0 0 100px 50px #000;font-size:20px}.c-link__close{position:absolute;right:10px;top:-40px;width:30px;fill:#fff;box-shadow:0 0 5px 1px #fff;border-radius:5px;cursor:pointer}.blur{filter:blur(5px)}.basic-component{position:relative;background-image:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");min-height:100vh;background-repeat:no-repeat;background-size:cover;color:#fff}.conteeeent{transition:.2s}.c-title-box{padding-top:10px;text-align:center}.c-title-box__title{font-size:60px}.c-title-box__subtitle{margin-top:5px;font-size:20px;color:hsla(0,0%,100%,.507)}.c-content{margin-top:30px;justify-content:space-around}.c-content,.c-content-text-box{display:flex;align-items:center}.c-content-text-box{position:relative;text-align:center;padding:10px;border-radius:10px;flex-direction:column;background-color:rgba(0,0,0,.274)}.c-content-text-box__title{color:hsla(0,0%,100%,.712);font-size:20px}.c-content-text-box__input{margin-top:10px;border-radius:4px;height:35px;color:#fff;font-size:20px;border:2px solid #fff;width:100%;padding:10px 0 10px 17px;background:rgba(0,0,0,.116)}.c-content-text-box__input::-moz-placeholder{font-family:\"MyriadPro-Regular\";color:hsla(0,0%,100%,.836)}.c-content-text-box__input:-ms-input-placeholder{font-family:\"MyriadPro-Regular\";color:hsla(0,0%,100%,.836)}.c-content-text-box__input::placeholder{font-family:\"MyriadPro-Regular\";color:hsla(0,0%,100%,.836)}.c-content-text-box__button{transition:.4s;margin-top:10px;outline:#fff;padding:7px 12px;width:50%;border-radius:5px;background:linear-gradient(90deg,#ff2a75,#00096b)}.c-content-text-box__button:hover{box-shadow:0 0 10px 1px #000;color:#fff}.rules{background-color:rgba(0,0,0,.308);position:absolute;border-radius:10px;bottom:-120px;left:0;padding:10px;font-size:18px;color:hsla(0,0%,78.8%,.747);font-style:italic}.rules span{color:#fff}@media(max-width:980px){.c-content{display:flex;flex-direction:column;align-items:center;margin-top:10px;transform:translate(0)}.c-content__image{width:500px}.c-content-text-box{width:95%;margin-top:25px}.c-content-text-box__button{opacity:1;background:linear-gradient(90deg,#ff2a75,#00096b);color:#fff}.rules{margin-top:15px;position:unset}}@media(max-width:580px){.c-content__image{width:300px}.c-link__hero{font-size:13px}.c-title-box{text-align:right;padding-right:10px}.c-title-box__title{font-size:45px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 53:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/first-bcg.67095c2.jpg";

/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/catalog-chats/v-open-chat.vue?vue&type=template&id=353426fa&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"basic-component"},[_vm._ssrNode("<div"+(_vm._ssrClass("conteeeent",{'blur': _vm.active_link}))+"><div class=\"c-title-box\"><h1 class=\"c-title-box__title\">"+_vm._ssrEscape(_vm._s(_vm.chat.name))+"</h1> <p class=\"c-title-box__subtitle\">Отличный выбор ;)</p></div> <div class=\"c-content\"><img"+(_vm._ssrAttr("src",_vm.chat.image))+" alt=\"chat-image\" class=\"c-content__image\"> <div class=\"c-content-text-box\"><h2 class=\"c-content-text-box__title\">Просто введи свой никнейм Твича, и наслождайся</h2> <input placeholder=\"BiG_RoBby\" type=\"text\""+(_vm._ssrAttr("value",(_vm.nickname)))+" class=\"c-content-text-box__input\"> <button class=\"c-content-text-box__button\">Получить ссылку</button> <div class=\"rules\"><p>Внимение!</p> <p>Если ваш никнейм <span>BiG_RoBby</span></p> <p>То никнейм <span>big_robby - не подойдет</span>. Чат чувтсвителен к регистру</p></div></div></div></div> "+((_vm.active_link)?("<div class=\"c-link\"><p class=\"c-link__hero\">"+_vm._ssrEscape(_vm._s(("http://localhost:3000/chats/" + (this.$route.params.chat_type) + "/" + _vm.nickname)))+"</p> <svg version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 26 26\" xml:space=\"preserve\" class=\"c-link__close\" style=\"enable-background:new 0 0 26 26;\"><g><path d=\"M21.125,0H4.875C2.182,0,0,2.182,0,4.875v16.25C0,23.818,2.182,26,4.875,26h16.25 C23.818,26,26,23.818,26,21.125V4.875C26,2.182,23.818,0,21.125,0z M18.78,17.394l-1.388,1.387c-0.254,0.255-0.67,0.255-0.924,0 L13,15.313L9.533,18.78c-0.255,0.255-0.67,0.255-0.925-0.002L7.22,17.394c-0.253-0.256-0.253-0.669,0-0.926l3.468-3.467 L7.221,9.534c-0.254-0.256-0.254-0.672,0-0.925l1.388-1.388c0.255-0.257,0.671-0.257,0.925,0L13,10.689l3.468-3.468 c0.255-0.257,0.671-0.257,0.924,0l1.388,1.386c0.254,0.255,0.254,0.671,0.001,0.927l-3.468,3.467l3.468,3.467 C19.033,16.725,19.033,17.138,18.78,17.394z\" style=\"fill:#030104;\"></path></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></div>"):"<!---->"))])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/catalog-chats/v-open-chat.vue?vue&type=template&id=353426fa&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/catalog-chats/v-open-chat.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var v_open_chatvue_type_script_lang_js_ = ({
  data() {
    return {
      nickname: '',
      active_link: false
    };
  },

  props: {
    chat: {
      type: Object,

      default() {
        return {};
      }

    }
  },
  methods: {
    getLink() {
      if (this.nickname.length) {
        this.active_link = !this.active_link;
      }
    },

    closeLink() {
      this.active_link = false;
    }

  }
});
// CONCATENATED MODULE: ./components/catalog-chats/v-open-chat.vue?vue&type=script&lang=js&
 /* harmony default export */ var catalog_chats_v_open_chatvue_type_script_lang_js_ = (v_open_chatvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/catalog-chats/v-open-chat.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(51)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  catalog_chats_v_open_chatvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "74042a92"
  
)

/* harmony default export */ var v_open_chat = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/chats/_chat_type/index.vue?vue&type=template&id=1d939f73&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('v-open-chat',{attrs:{"chat":_vm.chat_info}})}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/chats/_chat_type/index.vue?vue&type=template&id=1d939f73&

// EXTERNAL MODULE: ./components/catalog-chats/v-open-chat.vue + 4 modules
var v_open_chat = __webpack_require__(88);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/chats/_chat_type/index.vue?vue&type=script&lang=js&
//
//
//
//
//

/* harmony default export */ var _chat_typevue_type_script_lang_js_ = ({
  layout: 'logo',
  components: {
    vOpenChat: v_open_chat["default"]
  },

  data() {
    return {
      chat_info: {}
    };
  },

  mounted() {
    let a = 0;
    this.$store.getters['chat/CHATS'].forEach(item => {
      if (item.link == this.$route.params.chat_type) {
        this.chat_info = item;
        a++;
      }
    });

    if (a < 1) {
      this.$router.push({
        path: `/error`
      });
    }
  }

});
// CONCATENATED MODULE: ./pages/chats/_chat_type/index.vue?vue&type=script&lang=js&
 /* harmony default export */ var chats_chat_typevue_type_script_lang_js_ = (_chat_typevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./pages/chats/_chat_type/index.vue



function injectStyles (context) {
  
  
}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  chats_chat_typevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "33d033ff"
  
)

/* harmony default export */ var _chat_type = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=index.js.map
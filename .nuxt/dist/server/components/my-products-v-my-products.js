exports.ids = [8];
exports.modules = {

/***/ 50:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(85);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("82830b62", content, true, context)
};

/***/ }),

/***/ 80:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/preview.8a20892.png";

/***/ }),

/***/ 81:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/soap.4de640b.png";

/***/ }),

/***/ 82:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/decoration.be6dff1.png";

/***/ }),

/***/ 83:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/telegram.45256b0.png";

/***/ }),

/***/ 84:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_my_products_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(50);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_my_products_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_my_products_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_my_products_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_my_products_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 85:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(15);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(86);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".v-my_products{padding:100px 0 0;min-height:100vh;font-family:\"Facon\";color:#fbffff;background-image:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");background-repeat:no-repeat;background-size:cover}.v-my_products__content__title{font-size:50px;text-align:center;text-shadow:#000 -2px 1px 4px}.v-my_products__top{position:relative;display:flex;justify-content:space-between}.v-my_products__bottom{display:flex;justify-content:center}.v-my_products__product-item{position:relative;overflow:hidden;z-index:2;transition:.3s;cursor:pointer}.v-my_products__product-item__text{position:absolute;left:10%;top:50%;transform:translateY(-50%);text-shadow:#000 -6px 8px 4px;-ms-writing-mode:tb-lr;writing-mode:vertical-lr;-webkit-text-orientation:upright;text-orientation:upright;transition:.3s}.v-my_products__product-item:hover{transform:scale(1.1)}.v-my_products__product-item:hover .v-my_products__product-item__text{position:absolute;left:70%;transform:translateY(-50%)}.v-my_products__soap{position:absolute;left:50%;top:50%;transform:translate(-50%,-50%)}.v-my_products__preview{font-size:72px}.v-my_products__decoration{font-size:48px}.v-my_products__telegram{cursor:pointer;z-index:3;text-align:center;margin-top:-60px}.v-my_products__telegram__title{transition:.3s;font-size:72px;text-shadow:#000 6px 9px 10px}.v-my_products__telegram__image{transition:.3s}.v-my_products__telegram:hover .v-my_products__telegram__title{transform:scale(.9)}.v-my_products__telegram:hover .v-my_products__telegram__image{transform:scale(1.1)}@media(max-width:1025px){.v-my_products__preview{font-size:50px}.v-my_products__decoration{font-size:31px}.v-my_products__product-item__image{width:300px}.v-my_products__soap__image{width:522px}.v-my_products__telegram{margin-top:-20px}.v-my_products__telegram__title{font-size:50px}}@media(max-width:950px){.v-my_products__soap__image{width:322px}}@media(max-width:700px){.v-my_products__preview{font-size:40px}.v-my_products__decoration{font-size:24px}.v-my_products__product-item__image{width:250px}.v-my_products__telegram__title{font-size:40px}.v-my_products__telegram__image{width:200px}}@media(max-width:520px){.v-my_products__content__title{font-size:30px;text-align:center;text-shadow:#000 -2px 1px 4px}.v-my_products__preview{font-size:36px}.v-my_products__decoration{font-size:21px}.v-my_products__product-item__image{width:200px}.v-my_products__soap__image{width:242px}.v-my_products__telegram{margin-top:0}.v-my_products__telegram__title{font-size:30px}.v-my_products__telegram__image{width:150px}}@media(max-width:420px){.v-my_products__content__title{font-size:20px;text-align:center;text-shadow:#000 -2px 1px 4px}.v-my_products__preview{font-size:27px}.v-my_products__decoration{font-size:14px}.v-my_products__product-item__image{width:140px}.v-my_products__soap__image{width:200px}.v-my_products__telegram{margin-top:0}.v-my_products__telegram__title{font-size:20px}.v-my_products__telegram__image{width:100px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 86:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/bcg1.57ab2c4.jpg";

/***/ }),

/***/ 95:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/my_products/v-my-products.vue?vue&type=template&id=44ab0148&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"v-my_products",attrs:{"id":"v-my_products"}},[_vm._ssrNode("<div class=\"container\"><div class=\"v-my_products__content\"><h3 class=\"v-my_products__content__title\">Закажите у нас</h3> <div data-aos=\"flip-left\" data-aos-delay=\"300\" data-aos-once=\"true\" data-aos-duration=\"700\" class=\"v-my_products__top\"><div class=\"v-my_products__preview v-my_products__product-item\"><p class=\"v-my_products__product-item__text\">Превью</p> <img"+(_vm._ssrAttr("src",__webpack_require__(80)))+" alt class=\"v-my_products__product-item__image\"></div> <div class=\"v-my_products__soap\"><img"+(_vm._ssrAttr("src",__webpack_require__(81)))+" alt class=\"v-my_products__soap__image\"></div> <div class=\"v-my_products__decoration v-my_products__product-item\"><p class=\"v-my_products__product-item__text\">Оформление</p> <img"+(_vm._ssrAttr("src",__webpack_require__(82)))+" alt class=\"v-my_products__product-item__image\"></div></div> <div class=\"v-my_products__bottom\"><div class=\"v-my_products__telegram\"><h2 class=\"v-my_products__telegram__title\">Бот Телеграм</h2> <div class=\"v-my_products__telegram__image-box\"><img"+(_vm._ssrAttr("src",__webpack_require__(83)))+" alt class=\"v-my_products__telegram__image\"></div></div></div></div></div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/my_products/v-my-products.vue?vue&type=template&id=44ab0148&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/my_products/v-my-products.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var v_my_productsvue_type_script_lang_js_ = ({});
// CONCATENATED MODULE: ./components/my_products/v-my-products.vue?vue&type=script&lang=js&
 /* harmony default export */ var my_products_v_my_productsvue_type_script_lang_js_ = (v_my_productsvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/my_products/v-my-products.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(84)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  my_products_v_my_productsvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "06b5e951"
  
)

/* harmony default export */ var v_my_products = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=my-products-v-my-products.js.map
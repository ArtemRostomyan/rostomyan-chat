exports.ids = [4];
exports.modules = {

/***/ 37:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/fire.c937486.gif";

/***/ }),

/***/ 38:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/diamond.c7ce40f.gif";

/***/ }),

/***/ 39:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/robot.26fbc8e.gif";

/***/ }),

/***/ 46:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/side.45c4f93.png";

/***/ }),

/***/ 47:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(65);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("67f8f2a4", content, true, context)
};

/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/верх.4daa264.png";

/***/ }),

/***/ 63:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/низ.b13fa71.png";

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_gray_chat_vue_vue_type_style_index_0_id_518a5b01_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(47);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_gray_chat_vue_vue_type_style_index_0_id_518a5b01_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_gray_chat_vue_vue_type_style_index_0_id_518a5b01_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_gray_chat_vue_vue_type_style_index_0_id_518a5b01_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_gray_chat_vue_vue_type_style_index_0_id_518a5b01_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 65:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".chat[data-v-518a5b01]{position:relative;overflow:hidden;width:100%;height:100vh;margin:0 auto;background-repeat:no-repeat;padding:10px}.chat[data-v-518a5b01]:before{position:absolute;content:\"\";border-radius:10px;width:100%;z-index:2;height:20px;background-color:#c9fcfa;box-shadow:0 0 500px 45px #355a98;top:0;left:0}.chat__content[data-v-518a5b01]{z-index:1;position:absolute;overflow:hidden;background:linear-gradient(90deg,#d7d7d7,#fff);width:91%;min-height:100%;left:23px;bottom:0;padding:10px 25px 95px;border-radius:30px}@media(max-width:480px){.chat__content[data-v-518a5b01]{padding:10px 25px 95px;width:95%;left:10px;bottom:10px}}.chat__item[data-v-518a5b01]{display:flex;align-items:center;margin:10px 0;background:#241c52;background:linear-gradient(90deg,#696969,#aeafaf);padding:10px;border-radius:20px;-webkit-animation:appearance-item-data-v-518a5b01 .7s ease 1 forwards;animation:appearance-item-data-v-518a5b01 .7s ease 1 forwards}.chat__top[data-v-518a5b01]{top:0}.chat__bottom[data-v-518a5b01],.chat__top[data-v-518a5b01]{position:absolute;z-index:3;left:0;width:100%}.chat__bottom[data-v-518a5b01]{bottom:0}.chat__left[data-v-518a5b01]{left:8px;border-radius:0 0 0 176px}.chat__left[data-v-518a5b01],.chat__right[data-v-518a5b01]{position:absolute;z-index:2;height:72%;top:113px;width:21px}.chat__right[data-v-518a5b01]{right:11px;border-radius:0 0 175px 0}.name[data-v-518a5b01]{position:relative;background-color:hsla(0,0%,100%,0);border-radius:10px;padding:10px;color:#fff;margin-left:6px;font-weight:700;font-size:22px}@media(max-width:480px){.name[data-v-518a5b01]{margin-left:0}}.message[data-v-518a5b01]{max-width:950px;overflow-x:hidden;font-size:22px;color:#fff;background-color:hsla(0,0%,100%,0);font-weight:600;font-style:italic;display:flex;align-items:center;padding:10px;border-radius:20px;margin:0 0 0 20px;word-wrap:break-word;word-break:normal;line-break:strict;-webkit-hyphens:auto;-ms-hyphens:auto;hyphens:auto}.message__img[data-v-518a5b01]{width:40px}.elit[data-v-518a5b01]{position:absolute;background-color:rgba(0,0,0,.5);top:50%;transform:translateY(-50%);border-radius:40%;width:23px;height:23px}.subscriber[data-v-518a5b01]{position:relative;left:-35px}.subscriber__image[data-v-518a5b01]{box-shadow:0 0 10px 1px #000}@media(max-width:480px){.subscriber[data-v-518a5b01]{top:-2px;left:-23px}}.subscriber__stazh[data-v-518a5b01]{position:absolute;right:0;bottom:0}.moder[data-v-518a5b01]{left:-24px;top:40px;box-shadow:0 0 10px 1px #ff46f0}@media(max-width:480px){.moder[data-v-518a5b01]{left:-13px;top:33px}}.nightBot[data-v-518a5b01]{width:40px;height:40px;transition:.3s;left:-24px;background-color:hsla(0,0%,100%,.37);-webkit-animation:bot-data-v-518a5b01 .3s ease 1 forwards;animation:bot-data-v-518a5b01 .3s ease 1 forwards}@media(max-width:480px){.nightBot[data-v-518a5b01]{left:-10px}}.gun[data-v-518a5b01]{position:absolute;width:40px;height:40px;left:4px;bottom:30px}.gun__active[data-v-518a5b01]{transform:rotate(-70deg);-webkit-animation:gun_shot .1s ease 1 forwards;animation:gun_shot .1s ease 1 forwards}@media(max-width:480px){.gun[data-v-518a5b01]{width:30px;height:30px;left:4px;bottom:5px;transform:rotate(-45deg)}}.active__mod[data-v-518a5b01],.active__sub[data-v-518a5b01]{border:2px solid #3b90ff;color:#000;box-shadow:0 0 10px 1px #3b90ff;background-color:#fff}@-webkit-keyframes bot-data-v-518a5b01{0%{width:50px;height:50px}to{width:30px;height:30px}}@keyframes bot-data-v-518a5b01{0%{width:50px;height:50px}to{width:30px;height:30px}}@-webkit-keyframes appearance-item-data-v-518a5b01{0%{transform:scaleX(0);background-color:#331414}to{transform:scaleX(1);background-color:rgba(0,0,0,.16)}}@keyframes appearance-item-data-v-518a5b01{0%{transform:scaleX(0);background-color:#331414}to{transform:scaleX(1);background-color:rgba(0,0,0,.16)}}.bok[data-v-518a5b01]{z-index:2}@media(max-height:750px){.bok[data-v-518a5b01]{height:63%}}@media(max-height:700px){.bok[data-v-518a5b01]{height:64%}}@media(max-height:650px){.bok[data-v-518a5b01]{height:63%}}@media(max-height:600px){.bok[data-v-518a5b01]{height:60%}}@media(max-height:550px){.bok[data-v-518a5b01]{height:57%}}@media(max-height:500px){.bok[data-v-518a5b01]{height:55%}}@media(max-height:450px){.bok[data-v-518a5b01]{height:50%}}@media(min-height:868px)and (max-height:900px){.bok[data-v-518a5b01]{height:73%}}@media(min-height:901px)and (max-height:1000px){.bok[data-v-518a5b01]{height:75%}}@media(min-height:1001px)and (max-height:1100px){.bok[data-v-518a5b01]{height:77%}}@media(min-height:1101px)and (max-height:1200px){.bok[data-v-518a5b01]{height:79%}}@media(min-height:1201px)and (max-height:1300px){.bok[data-v-518a5b01]{height:81%}}@media(min-height:1301px){.bok[data-v-518a5b01]{height:82%}}.link-for-site[data-v-518a5b01]{color:#fff;width:100%;font-family:\"MellaNissa\";z-index:99;position:absolute;text-align:center;bottom:16px;text-shadow:0 0 4px #8482ff;font-size:22px}.link-for-site span[data-v-518a5b01]{color:#fff;text-shadow:0 0 4px #000}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/chat/v-gray-chat.vue?vue&type=template&id=518a5b01&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"chat"},[_vm._ssrNode("<img"+(_vm._ssrAttr("src",__webpack_require__(62)))+" alt class=\"chat__top\" data-v-518a5b01> <img"+(_vm._ssrAttr("src",__webpack_require__(46)))+" alt class=\"chat__left bok\" data-v-518a5b01> <div class=\"chat__content\" data-v-518a5b01>"+(_vm._ssrList((_vm.chat),function(komment,index){return ("<div class=\"chat__item\" data-v-518a5b01><div"+(_vm._ssrClass("name",{'active__sub': komment.subscriber, 'active__mod' : komment.mod}))+" data-v-518a5b01><div class=\"subscriber\" data-v-518a5b01>"+((komment.subscriber == true )?("<img"+(_vm._ssrAttr("src",__webpack_require__(37)))+" alt class=\"elit subscriber__image\" data-v-518a5b01>"):"<!---->")+" "+((komment['badge-info.subscriber'])?("<span class=\"subscriber__stazh\" data-v-518a5b01>"+_vm._ssrEscape(_vm._s(komment['badge-info.subscriber']))+"</span>"):"<!---->")+"</div> "+((komment.mod == true &&  komment['display-name'] != 'Nightbot' &&  komment['display-name'] != 'StreamElements' &&  komment['display-name'] != 'Moobot' )?("<img"+(_vm._ssrAttr("src",__webpack_require__(38)))+" alt class=\"elit moder\" data-v-518a5b01>"):"<!---->")+" "+((komment['display-name'] === 'Nightbot' || komment['display-name'] === 'StreamElements' || komment['display-name'] === 'Moobot' )?("<img"+(_vm._ssrAttr("src",__webpack_require__(39)))+" alt class=\"elit nightBot\" data-v-518a5b01>"):"<!---->")+_vm._ssrEscape("\n                        "+_vm._s(komment['display-name']))+"</div> <div"+(_vm._ssrClass("message",{'active__sub': komment.subscriber  , 'active__mod' : komment.mod}))+" data-v-518a5b01>"+(_vm._s(_vm.emotionalKomment(komment)))+"</div></div>")}))+"</div> <img"+(_vm._ssrAttr("src",__webpack_require__(46)))+" alt class=\"chat__right bok\" data-v-518a5b01> <img"+(_vm._ssrAttr("src",__webpack_require__(63)))+" alt class=\"chat__bottom\" data-v-518a5b01> <p class=\"link-for-site\" data-v-518a5b01>Rostomyan - inc.com</p>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/chat/v-gray-chat.vue?vue&type=template&id=518a5b01&scoped=true&

// EXTERNAL MODULE: external "tmi.js"
var external_tmi_js_ = __webpack_require__(34);
var external_tmi_js_default = /*#__PURE__*/__webpack_require__.n(external_tmi_js_);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/chat/v-gray-chat.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var v_gray_chatvue_type_script_lang_js_ = ({
  props: {
    nick: {
      type: String,

      default() {
        return '';
      }

    }
  },
  name: 'v-chat',

  data() {
    return {
      gun_active: false
    };
  },

  methods: {
    emotionalKomment(komment) {
      if (!komment.emotes) {
        return komment.message;
      } else {
        const stringReplacements = [];
        Object.entries(komment.emotes).forEach(([id, positions]) => {
          const position = positions[0];
          const [start, end] = position.split("-");
          const stringToReplace = komment.message.substring(parseInt(start, 10), parseInt(end, 10) + 1);
          stringReplacements.push({
            stringToReplace: stringToReplace,
            replacement: ` <img style="width: 40px;" src="https://static-cdn.jtvnw.net/emoticons/v1/${id}/3.0"> `
          });
        });
        const messageHTML = stringReplacements.reduce((acc, {
          stringToReplace,
          replacement
        }) => {
          return acc.split(stringToReplace).join(replacement);
        }, komment.message);
        return messageHTML;
      }
    }

  },
  computed: {
    chat() {
      return this.$store.getters['chat/CHAT'];
    }

  },

  mounted() {
    const client = new external_tmi_js_default.a.Client({
      connection: {
        reconnect: true
      },
      channels: [this.nick]
    });
    client.connect();
    client.on('message', (channel, tags, message, self) => {
      this.gun_active = !this.gun_active;
      let new_koment = tags;

      if (tags['display-name'] == "dragnisimus") {
        tags.subscriber = true;
        tags.mod = true;
      }

      if (tags.subscriber == true) {
        console.log(tags);
      }

      tags.message = message;
      this.$store.commit('chat/SET_CHAT_IN_STATE', new_koment, message);
    });
  }

});
// CONCATENATED MODULE: ./components/chat/v-gray-chat.vue?vue&type=script&lang=js&
 /* harmony default export */ var chat_v_gray_chatvue_type_script_lang_js_ = (v_gray_chatvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/chat/v-gray-chat.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(64)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  chat_v_gray_chatvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "518a5b01",
  "7833f44f"
  
)

/* harmony default export */ var v_gray_chat = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=chat-v-gray-chat.js.map
exports.ids = [1];
exports.modules = {

/***/ 49:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(78);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("d3b95676", content, true, context)
};

/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/top_materials.36060c6.jpg";

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_chat_catalog_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(49);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_chat_catalog_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_chat_catalog_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_chat_catalog_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_chat_catalog_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(15);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(79);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".chats-component{position:relative;background-image:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");background-size:cover;font-family:\"MyriadPro-Regular\";overflow-x:hidden;padding-bottom:70px}.chats-component__top-image{-o-object-fit:cover;object-fit:cover;box-shadow:0 28px 60px 26px #000;width:100%;height:45px}.chats-component__catalog{z-index:2;grid-row-gap:100px;row-gap:100px;grid-column-gap:97px;-moz-column-gap:97px;column-gap:97px;display:flex;justify-content:space-around;flex-wrap:wrap;margin-top:70px}@media(max-width:1209px){.chats-component__catalog{grid-column-gap:30px;-moz-column-gap:30px;column-gap:30px}}.chats-component__middle-strip{z-index:1;width:100%;position:absolute;top:50%;left:-5%;height:250px;transform:translateY(-65%) rotate(-4deg);background:#f07f9b;background:linear-gradient(90deg,#f07f9b,#23253c 45%);box-shadow:0 0 50px 10px #000;transform-origin:center center;width:200%}.chat-item{position:relative;-webkit-tap-highlight-color:rgba(0,0,0,0);padding:0 10px 10px;border-radius:5px;z-index:2;display:flex;flex-direction:column;align-items:center;justify-content:space-between;color:#fff;width:341px;cursor:pointer}.chat-item:hover{transition:.1s;background-color:rgba(0,0,0,.418);box-shadow:0 0 20px 1px #fff}.chat-item:active{background-color:rgba(0,0,0,.795)}@media(max-width:991px){.chat-item__kitty{margin-top:-30px}}@media(max-width:1650px){.chat-item{width:280px}}.chat-item__image{width:341px}@media(max-width:1650px){.chat-item__image{width:280px}}.chat-item__name{font-size:40px}@media(max-width:1650px){.chat-item__name{font-size:35px}}.chat-item__info{margin-top:15px;font-size:30px}@media(max-width:1650px){.chat-item__info{font-size:20px}}.chat-item__sizes{margin-top:20px;font-size:30px}@media(max-width:1650px){.chat-item__sizes{font-size:20px}}.chat-item__sizes-box{margin-left:30px}@media(max-width:725px){.chats-component__catalog{grid-column-gap:70px;-moz-column-gap:70px;column-gap:70px}.chat-item,.chat-item__image{width:200px}.chat-item__name{font-size:25px}.chat-item__info{font-size:16px}.chat-item__sizes{font-size:15px}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 79:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/big-bcg.8864f1d.jpg";

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/catalog-chats/v-chat-catalog.vue?vue&type=template&id=535e10ca&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"chats-component",attrs:{"id":"chats"}},[_vm._ssrNode("<img"+(_vm._ssrAttr("src",__webpack_require__(76)))+" alt class=\"chats-component__top-image\"> <figure class=\"chats-component__middle-strip\"></figure> "),_vm._ssrNode("<div class=\"container\">","</div>",[_vm._ssrNode("<div class=\"chats-component__catalog\" style=\"paddingBottom: 25px;\">","</div>",_vm._l((_vm.chats),function(chat,index){return _c('nuxt-link',{key:index,staticClass:"chats-component__item chat-item",class:chat.dop_class,attrs:{"to":("/chats/" + (chat.link)),"no-prefetch":"","data-aos":"flip-left","data-aos-delay":"100","data-aos-once":"true","data-aos-duration":"700"}},[_c('div',{staticClass:"chat-item__image-box"},[_c('img',{staticClass:"chat-item__image",attrs:{"src":chat.image,"alt":""}})]),_vm._v(" "),_c('div',{staticClass:"inffooooo"},[_c('div',{staticClass:"chat-item__info-box"},[_c('p',{staticClass:"chat-item__name"},[_vm._v(_vm._s(chat.name))]),_vm._v(" "),_c('p',{staticClass:"chat-item__info"},[_vm._v(_vm._s(chat.description))])]),_vm._v(" "),_c('div',{staticClass:"chat-item__sizes"},[_c('p',{staticClass:"chat-item__title"},[_vm._v("Рекомендованый размер :")]),_vm._v(" "),_c('div',{staticClass:"chat-item__sizes-box"},_vm._l((chat.sizes),function(size,index){return _c('p',{key:index,staticClass:"chat-item__width"},[_vm._v(_vm._s(size.name)+": "+_vm._s(size.value)+" px")])}),0)])])])}),1),_vm._ssrNode(" <a href=\"#v-my_products\" class=\"arrow-box scroll-to\"><svg version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 443.52 443.52\" xml:space=\"preserve\" class=\"arrow-box__arrow\" style=\"enable-background:new 0 0 443.52 443.52;\"><g><g><path d=\"M336.226,209.591l-204.8-204.8c-6.78-6.548-17.584-6.36-24.132,0.42c-6.388,6.614-6.388,17.099,0,23.712l192.734,192.734 L107.294,414.391c-6.663,6.664-6.663,17.468,0,24.132c6.665,6.663,17.468,6.663,24.132,0l204.8-204.8 C342.889,227.058,342.889,216.255,336.226,209.591z\"></path></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></a>")],2)],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/catalog-chats/v-chat-catalog.vue?vue&type=template&id=535e10ca&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/catalog-chats/v-chat-catalog.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var v_chat_catalogvue_type_script_lang_js_ = ({
  mounted() {
    const anchors = document.querySelectorAll('a.scroll-to');

    for (let anchor of anchors) {
      anchor.addEventListener('click', function (e) {
        e.preventDefault();
        const blockID = anchor.getAttribute('href');
        document.querySelector(blockID).scrollIntoView({
          behavior: 'smooth',
          block: 'start'
        });
      });
    }
  },

  data() {
    return {};
  },

  methods: {
    OpenChat(chat) {
      this.$router.push({
        path: `/chats/${chat.link}`
      });
    }

  },
  computed: {
    chats() {
      return this.$store.getters['chat/CHATS'];
    }

  }
});
// CONCATENATED MODULE: ./components/catalog-chats/v-chat-catalog.vue?vue&type=script&lang=js&
 /* harmony default export */ var catalog_chats_v_chat_catalogvue_type_script_lang_js_ = (v_chat_catalogvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/catalog-chats/v-chat-catalog.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(77)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  catalog_chats_v_chat_catalogvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "d463e7d0"
  
)

/* harmony default export */ var v_chat_catalog = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=catalog-chats-v-chat-catalog.js.map
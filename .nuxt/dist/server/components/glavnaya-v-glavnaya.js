exports.ids = [7];
exports.modules = {

/***/ 48:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(74);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("5267693a", content, true, context)
};

/***/ }),

/***/ 66:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/stone_left.9702411.png";

/***/ }),

/***/ 67:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/stone_right.537a0b5.png";

/***/ }),

/***/ 68:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/stone_super_rigth.0d6238a.png";

/***/ }),

/***/ 69:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/kitty.9c64969.png";

/***/ }),

/***/ 70:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/gray.e3c97e3.png";

/***/ }),

/***/ 71:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/basic.07907a4.png";

/***/ }),

/***/ 72:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/neon.5dcbd22.png";

/***/ }),

/***/ 73:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_glavnaya_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(48);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_glavnaya_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_glavnaya_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_glavnaya_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_glavnaya_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 74:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(15);
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(75);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".chats{margin-left:20px;margin-top:-50%;transform:translateY(50%);position:relative;z-index:3;display:flex}@media(max-width:1260px){.chats{margin-left:0}}@media(max-width:1200px){.chats{transform:scale(.8) translate(0);margin-top:0}}@media(max-width:880px){.chats{transform:scale(.7)}}@media(max-width:567px){.chats{margin-top:-40px;transform:scale(.5)}}@media(max-width:430px){.chats{transform:scale(.4)}}@media(max-width:880px)and (max-height:540px){.chats{transform:scale(.5);margin:-65px -280px 0 0}}@media(max-width:880px)and (max-height:415px){.chats{transform:scale(.4);margin:-100px -280px 0 0}}@media(max-width:1570px){.chats__image:not(:last-child){width:300px}}.kitty-image{z-index:3;margin-right:-47px;transform:translate(-1000%);-webkit-animation:kittyChat .7s ease 2s forwards;animation:kittyChat .7s ease 2s forwards}@-webkit-keyframes kittyChat{0%{transform:translate(-1000%)}to{transform:translate(0)}}@keyframes kittyChat{0%{transform:translate(-1000%)}to{transform:translate(0)}}.gray-image{z-index:4;text-shadow:0 0 10px #000;transform:translateY(-1000%);-webkit-animation:grayChat .7s ease 1.6s forwards;animation:grayChat .7s ease 1.6s forwards}@-webkit-keyframes grayChat{0%{transform:translateY(-1000%)}to{transform:translate(0)}}@keyframes grayChat{0%{transform:translateY(-1000%)}to{transform:translate(0)}}.basic-image{margin-left:-47px;z-index:3;transform:translate(1000%);-webkit-animation:basicChat .7s ease 2s forwards;animation:basicChat .7s ease 2s forwards}@-webkit-keyframes basicChat{0%{transform:translate(1000%)}to{transform:translate(0)}}@keyframes basicChat{0%{transform:translate(1000%)}to{transform:translate(0)}}.neon-image{position:absolute;bottom:-150px;left:50%;transform:translateY(-1000%);z-index:2;-webkit-animation:neonChat .7s ease 2s forwards;animation:neonChat .7s ease 2s forwards}@-webkit-keyframes neonChat{0%{transform:translate(1000%)}to{transform:translate(-50%)}}@keyframes neonChat{0%{transform:translate(1000%)}to{transform:translate(-50%)}}.glavnaya{overflow:hidden;position:relative;width:100%;min-height:320px;background:url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") 0 0;background-repeat:no-repeat;background-size:cover}.glavnaya__subtitle{color:#ffe952;font-size:105px;transform:translate(-1000%);-webkit-animation:left_go .4s ease 1s forwards;animation:left_go .4s ease 1s forwards}@media(max-width:1570px){.glavnaya__subtitle{font-size:75px}}.glavnaya__title{color:#fff;font-size:78px;transform:translate(-1000%);-webkit-animation:left_go .4s ease 1.2s forwards;animation:left_go .4s ease 1.2s forwards}@media(max-width:1570px){.glavnaya__title{font-size:55px}}.arrow-box{position:absolute;cursor:pointer;opacity:0;-webkit-animation:opacityAnim .3s ease 1.8s forwards;animation:opacityAnim .3s ease 1.8s forwards;bottom:10px;-webkit-tap-highlight-color:rgba(0,0,0,0);left:50%;display:flex;justify-content:center;align-items:center;transform:translate(-50%) rotate(90deg);z-index:5;padding:10px;border:2px solid #fff;background-color:rgba(0,0,0,.27);border-radius:50%;box-shadow:0 0 20px 2px #fff}.arrow-box__arrow{width:20px;fill:#fff;filter:drop-shadow(-2px 0 2px white)}@media(max-width:680px){.arrow-box__arrow{width:15px}}@media(max-height:415px){.arrow-box__arrow{width:15px}}.content{display:flex;justify-content:space-between;padding:0 30px;align-items:center;height:100vh}@media(max-width:1260px){.content{display:flex;align-items:flex-start;justify-content:center}}@media(max-width:885px){.content{display:flex;justify-content:center}}.stone__left{height:70vh;position:absolute;bottom:0;left:0;z-index:2;-webkit-animation:stone_left 1s ease forwards;animation:stone_left 1s ease forwards}@media(max-width:880px){.stone__left{height:45vh}}@media(max-width:1025px)and (max-height:1400px){.stone__left{height:45vh}}.stone__rigth{height:32vh;position:absolute;bottom:-100%;left:30%;z-index:2;-webkit-animation:stone_rigth 1s ease .5s forwards;animation:stone_rigth 1s ease .5s forwards}@media(max-width:880px){.stone__rigth{height:25vh}}.stone__super-rigth{position:absolute;right:0;bottom:0;-webkit-animation:stone_super-rigth 1s ease forwards;animation:stone_super-rigth 1s ease forwards}.texts{z-index:3;font-family:\"Facon\";font-size:30px}@media(max-height:670px){.texts{position:absolute;bottom:0;left:0}}@media(max-height:470px){.texts{position:absolute;transform:scale(.4);bottom:-60px;left:-121px}}@media(max-width:1570px){.texts{font-size:20px}}@media(max-width:1260px){.texts{position:absolute;bottom:40px;left:40px}}@media(max-width:1200px){.texts{position:absolute;left:50%;transform:scale(.8) translate(-60%);bottom:40px}}@media(max-width:880px)and (max-height:650px){.texts{position:absolute;transform:scale(.6);left:0;bottom:0}}@media(max-width:880px)and (max-height:540px){.texts{position:absolute;transform:scale(.7)}}@media(max-width:880px)and (max-height:415px){.texts{position:absolute;transform:scale(.7)}}@media(max-width:880px)and (max-height:375px){.texts{position:absolute;transform:scale(.5);left:-100px;bottom:-40px}}.ponti{margin-top:60px;color:#fff;font-family:Arial;font-style:italic;transform:scaleY(0);-webkit-animation:scaleY .3s ease 1.8s forwards;animation:scaleY .3s ease 1.8s forwards}@media(max-width:1200px){.ponti{margin-top:10px}}.ponti__icons{margin-top:20px;display:flex}.ponti__icon{width:30px;height:30px;fill:#fff;transition:.7s}.ponti-icon-box{box-shadow:0 0 5px 1px #fff;-webkit-tap-highlight-color:rgba(0,0,0,0);width:45px;height:45px;display:flex;justify-content:center;align-items:center;background-color:rgba(0,0,0,.596);padding:10px;border-radius:50%;transition:all .3s ease;cursor:pointer}@media(max-width:1260px){.ponti-icon-box{width:40px;height:40px}}.ponti-icon-box:hover{background-color:#fff;box-shadow:0 0 10px 1px #fff}.ponti-icon-box:hover .ponti__icon{transition:.7s;fill:#000}.ponti-icon-box:not(:first-child){margin-left:15px}.ponti__texts{display:flex}.ponti__text{color:#fff;font-family:Arial;font-style:italic;position:relative;margin-left:15px}.ponti__text:before{position:absolute;content:\"\";width:100%;left:0;bottom:-5px;height:3px;background-color:#fff;transform:scaleX(0);-webkit-animation:BorderBottom 1s ease infinite;animation:BorderBottom 1s ease infinite}.ponti__subtext{position:relative}@-webkit-keyframes stone_left{0%{transform:rotate(90deg) scale(.1);position:absolute;left:-100%;bottom:-100%}to{transform:rotate(0deg) scale(1);position:absolute;left:0;bottom:0}}@keyframes stone_left{0%{transform:rotate(90deg) scale(.1);position:absolute;left:-100%;bottom:-100%}to{transform:rotate(0deg) scale(1);position:absolute;left:0;bottom:0}}@-webkit-keyframes stone_rigth{0%{transform:rotate(-90deg) scale(.1);position:absolute;bottom:-100%}to{transform:rotate(0deg) scale(1);position:absolute;bottom:0}}@keyframes stone_rigth{0%{transform:rotate(-90deg) scale(.1);position:absolute;bottom:-100%}to{transform:rotate(0deg) scale(1);position:absolute;bottom:0}}@-webkit-keyframes stone_super-rigth{0%{transform:rotate(-90deg) scale(.1);position:absolute;bottom:-100%}to{transform:rotate(0deg) scale(1);position:absolute;bottom:0}}@keyframes stone_super-rigth{0%{transform:rotate(-90deg) scale(.1);position:absolute;bottom:-100%}to{transform:rotate(0deg) scale(1);position:absolute;bottom:0}}@-webkit-keyframes left_go{0%{transform:translate(-1000%)}to{transform:translate(0)}}@keyframes left_go{0%{transform:translate(-1000%)}to{transform:translate(0)}}@-webkit-keyframes scaleY{0%{transform:scaleY(0)}to{transform:scaleY(1)}}@keyframes scaleY{0%{transform:scaleY(0)}to{transform:scaleY(1)}}@-webkit-keyframes BorderBottom{0%{transform:scaleX(0)}to{transform:scaleY(0)}}@keyframes BorderBottom{0%{transform:scaleX(0)}to{transform:scaleY(0)}}@-webkit-keyframes opacityAnim{0%{opacity:0}to{opacity:1}}@keyframes opacityAnim{0%{opacity:0}to{opacity:1}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 75:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/bc1.b8415fc.jpg";

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/glavnaya/v-glavnaya.vue?vue&type=template&id=1d7c1bf8&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"glavnaya"},[_vm._ssrNode("<div class=\"container\" style=\"max-width: 1500px;\"><img"+(_vm._ssrAttr("src",__webpack_require__(66)))+" alt class=\"stone__left\"> <img"+(_vm._ssrAttr("src",__webpack_require__(67)))+" alt class=\"stone__rigth\"> <img"+(_vm._ssrAttr("src",__webpack_require__(68)))+" alt class=\"stone__super-rigth\"> <div class=\"content\"><div class=\"texts\"><p class=\"glavnaya__subtitle\">Лучший</p> <h1 class=\"glavnaya__title\">Чат для твича</h1> <div class=\"ponti\"><div class=\"ponti__texts\"><p class=\"ponti__subtext\">Чат</p> <a href=\"https://vk.com/dragnisimus\" target=\"_blank\" class=\"ponti__text\">Ростомяна Артёма</a></div> <div class=\"ponti__icons\"><a href=\"https://vk.com/dragnisimus\" target=\"_blank\" class=\"ponti-icon-box\"><svg id=\"Bold\" enable-background=\"new 0 0 24 24\" height=\"512\" viewBox=\"0 0 24 24\" width=\"512\" xmlns=\"http://www.w3.org/2000/svg\" class=\"ponti__icon icon-vk\"><path d=\"m19.915 13.028c-.388-.49-.277-.708 0-1.146.005-.005 3.208-4.431 3.538-5.932l.002-.001c.164-.547 0-.949-.793-.949h-2.624c-.668 0-.976.345-1.141.731 0 0-1.336 3.198-3.226 5.271-.61.599-.892.791-1.225.791-.164 0-.419-.192-.419-.739v-5.105c0-.656-.187-.949-.74-.949h-4.126c-.419 0-.668.306-.668.591 0 .622.945.765 1.043 2.515v3.797c0 .832-.151.985-.486.985-.892 0-3.057-3.211-4.34-6.886-.259-.713-.512-1.001-1.185-1.001h-2.625c-.749 0-.9.345-.9.731 0 .682.892 4.073 4.148 8.553 2.17 3.058 5.226 4.715 8.006 4.715 1.671 0 1.875-.368 1.875-1.001 0-2.922-.151-3.198.686-3.198.388 0 1.056.192 2.616 1.667 1.783 1.749 2.076 2.532 3.074 2.532h2.624c.748 0 1.127-.368.909-1.094-.499-1.527-3.871-4.668-4.023-4.878z\"></path></svg></a> <a href=\"https://t.me/DisplayN0ne\" target=\"_blank\" class=\"ponti-icon-box\"><svg id=\"Bold\" enable-background=\"new 0 0 24 24\" height=\"512\" viewBox=\"0 0 24 24\" width=\"512\" xmlns=\"http://www.w3.org/2000/svg\" class=\"ponti__icon icon-telegram\"><path d=\"m9.417 15.181-.397 5.584c.568 0 .814-.244 1.109-.537l2.663-2.545 5.518 4.041c1.012.564 1.725.267 1.998-.931l3.622-16.972.001-.001c.321-1.496-.541-2.081-1.527-1.714l-21.29 8.151c-1.453.564-1.431 1.374-.247 1.741l5.443 1.693 12.643-7.911c.595-.394 1.136-.176.691.218z\"></path></svg></a></div></div></div> <div class=\"chats\"><img"+(_vm._ssrAttr("src",__webpack_require__(69)))+" alt class=\"chats__image kitty-image\"> <img"+(_vm._ssrAttr("src",__webpack_require__(70)))+" alt class=\"chats__image gray-image\"> <img"+(_vm._ssrAttr("src",__webpack_require__(71)))+" alt class=\"chats__image basic-image\"> <img"+(_vm._ssrAttr("src",__webpack_require__(72)))+" alt class=\"chats__image neon-image\"></div></div></div> <a href=\"#chats\" class=\"arrow-box scroll-to\"><svg version=\"1.1\" id=\"Capa_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 443.52 443.52\" xml:space=\"preserve\" class=\"arrow-box__arrow\" style=\"enable-background:new 0 0 443.52 443.52;\"><g><g><path d=\"M336.226,209.591l-204.8-204.8c-6.78-6.548-17.584-6.36-24.132,0.42c-6.388,6.614-6.388,17.099,0,23.712l192.734,192.734 L107.294,414.391c-6.663,6.664-6.663,17.468,0,24.132c6.665,6.663,17.468,6.663,24.132,0l204.8-204.8 C342.889,227.058,342.889,216.255,336.226,209.591z\"></path></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></a>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/glavnaya/v-glavnaya.vue?vue&type=template&id=1d7c1bf8&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/glavnaya/v-glavnaya.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var v_glavnayavue_type_script_lang_js_ = ({
  mounted() {
    const anchors = document.querySelectorAll('a.scroll-to');

    for (let anchor of anchors) {
      anchor.addEventListener('click', function (e) {
        e.preventDefault();
        const blockID = anchor.getAttribute('href');
        document.querySelector(blockID).scrollIntoView({
          behavior: 'smooth',
          block: 'start'
        });
      });
    }
  },

  methods: {}
});
// CONCATENATED MODULE: ./components/glavnaya/v-glavnaya.vue?vue&type=script&lang=js&
 /* harmony default export */ var glavnaya_v_glavnayavue_type_script_lang_js_ = (v_glavnayavue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/glavnaya/v-glavnaya.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(73)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  glavnaya_v_glavnayavue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "6a08160a"
  
)

/* harmony default export */ var v_glavnaya = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=glavnaya-v-glavnaya.js.map
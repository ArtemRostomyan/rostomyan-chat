exports.ids = [5];
exports.modules = {

/***/ 35:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/облачко1.6178a43.png";

/***/ }),

/***/ 36:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/облачко2.1748999.png";

/***/ }),

/***/ 37:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/fire.c937486.gif";

/***/ }),

/***/ 38:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/diamond.c7ce40f.gif";

/***/ }),

/***/ 39:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/robot.26fbc8e.gif";

/***/ }),

/***/ 40:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/guns.f1fa9cd.svg";

/***/ }),

/***/ 43:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(57);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("502e7224", content, true, context)
};

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_kitty_chat_vue_vue_type_style_index_0_id_cb8085ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(43);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_kitty_chat_vue_vue_type_style_index_0_id_cb8085ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_kitty_chat_vue_vue_type_style_index_0_id_cb8085ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_kitty_chat_vue_vue_type_style_index_0_id_cb8085ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_v_kitty_chat_vue_vue_type_style_index_0_id_cb8085ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 57:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".chat[data-v-cb8085ba]{position:relative;overflow:hidden;width:96%;height:90vh;top:75px;margin:0 auto;background-repeat:no-repeat;background-color:#a3d2e8;border:6px solid #fff;box-shadow:0 0 10px 2px #a3d2e8;border-radius:10px;padding:10px}.chat__content[data-v-cb8085ba]{z-index:1;position:absolute;overflow:hidden;width:93%;left:40px;bottom:0;padding:20px 25px}@media(max-width:480px){.chat__content[data-v-cb8085ba]{padding:10px 5px;width:95%;left:10px;bottom:10px}}.chat__item[data-v-cb8085ba]{display:flex;align-items:center;margin:10px 0;background-color:rgba(0,0,0,.63);padding:10px;border-radius:20px;-webkit-animation:appearance-item-data-v-cb8085ba .7s ease 1 forwards;animation:appearance-item-data-v-cb8085ba .7s ease 1 forwards}.chat__kitty[data-v-cb8085ba]{position:absolute;transform:translate(-50%,-50%);margin:0 auto}.name[data-v-cb8085ba]{position:relative;background-color:hsla(0,0%,100%,.45);border-radius:10px;padding:10px;margin-left:6px;font-weight:700;font-size:22px}@media(max-width:480px){.name[data-v-cb8085ba]{margin-left:0}}.message[data-v-cb8085ba]{max-width:950px;overflow-x:hidden;font-size:22px;color:#000;background-color:#fff;font-weight:600;font-style:italic;display:flex;align-items:center;padding:10px;border-radius:20px;margin:0 0 0 20px;word-wrap:break-word;word-break:normal;line-break:strict;-webkit-hyphens:auto;-ms-hyphens:auto;hyphens:auto}.message__img[data-v-cb8085ba]{width:40px}.elit[data-v-cb8085ba]{background-color:rgba(0,0,0,.5);top:50%;transform:translateY(-50%);border-radius:40%;width:23px;height:23px}.cloud[data-v-cb8085ba],.elit[data-v-cb8085ba]{position:absolute}.cloud--1[data-v-cb8085ba]{left:15%;top:19%}.cloud--1[data-v-cb8085ba],.cloud--2[data-v-cb8085ba]{position:fixed;-webkit-animation:first-animate-data-v-cb8085ba 10s ease infinite;animation:first-animate-data-v-cb8085ba 10s ease infinite}.cloud--2[data-v-cb8085ba]{left:11%;top:88%}.cloud--3[data-v-cb8085ba]{position:fixed;left:37%;top:49%;-webkit-animation:first-animate-data-v-cb8085ba 10s ease infinite;animation:first-animate-data-v-cb8085ba 10s ease infinite}.cloud--4[data-v-cb8085ba]{left:4%;top:62%}.cloud--4[data-v-cb8085ba],.cloud--5[data-v-cb8085ba]{position:fixed;-webkit-animation:threee-animate-data-v-cb8085ba 10s ease infinite;animation:threee-animate-data-v-cb8085ba 10s ease infinite}.cloud--5[data-v-cb8085ba]{left:63%;top:54%}.cloud--6[data-v-cb8085ba]{position:fixed;left:74%;top:18%;-webkit-animation:first-animate-data-v-cb8085ba 10s ease infinite;animation:first-animate-data-v-cb8085ba 10s ease infinite}.cloud--7[data-v-cb8085ba]{position:fixed;left:63%;top:84%;-webkit-animation:second-animate-data-v-cb8085ba 10s ease infinite;animation:second-animate-data-v-cb8085ba 10s ease infinite}.cloud--8[data-v-cb8085ba]{position:fixed;left:73%;top:72%;-webkit-animation:threee-animate-data-v-cb8085ba 10s ease infinite;animation:threee-animate-data-v-cb8085ba 10s ease infinite}.cloud--9[data-v-cb8085ba]{position:fixed;left:39%;top:70%;-webkit-animation:first-animate-data-v-cb8085ba 10s ease infinite;animation:first-animate-data-v-cb8085ba 10s ease infinite}.cloud--10[data-v-cb8085ba]{left:40%;top:11%}.cloud--10[data-v-cb8085ba],.cloud--11[data-v-cb8085ba]{position:fixed;-webkit-animation:threee-animate-data-v-cb8085ba 10s ease infinite;animation:threee-animate-data-v-cb8085ba 10s ease infinite}.cloud--11[data-v-cb8085ba]{left:12%;top:12%}.cloud--12[data-v-cb8085ba]{position:fixed;left:40%;top:32%;-webkit-animation:threee-animate-data-v-cb8085ba 10s ease infinite;animation:threee-animate-data-v-cb8085ba 10s ease infinite}.cloud--13[data-v-cb8085ba]{left:66%;top:38%}.cloud--13[data-v-cb8085ba],.cloud--14[data-v-cb8085ba]{position:fixed;-webkit-animation:first-animate-data-v-cb8085ba 10s ease infinite;animation:first-animate-data-v-cb8085ba 10s ease infinite}.cloud--14[data-v-cb8085ba]{left:42%;top:18%}.cloud--15[data-v-cb8085ba]{position:fixed;left:27%;top:28%;-webkit-animation:second-animate-data-v-cb8085ba 10s ease infinite;animation:second-animate-data-v-cb8085ba 10s ease infinite}.cloud--16[data-v-cb8085ba]{left:57%;top:33%}.cloud--16[data-v-cb8085ba],.cloud--17[data-v-cb8085ba]{position:fixed;-webkit-animation:threee-animate-data-v-cb8085ba 10s ease infinite;animation:threee-animate-data-v-cb8085ba 10s ease infinite}.cloud--17[data-v-cb8085ba]{left:15%;top:44%}.cloud--18[data-v-cb8085ba]{left:23%;top:57%}.cloud--18[data-v-cb8085ba],.cloud--19[data-v-cb8085ba]{position:fixed;-webkit-animation:second-animate-data-v-cb8085ba 10s ease infinite;animation:second-animate-data-v-cb8085ba 10s ease infinite}.cloud--19[data-v-cb8085ba]{left:4%;top:30%}.cloud--20[data-v-cb8085ba]{position:fixed;left:28%;top:80%;-webkit-animation:first-animate-data-v-cb8085ba 10s ease infinite;animation:first-animate-data-v-cb8085ba 10s ease infinite}.subscriber[data-v-cb8085ba]{position:relative;left:-35px}.subscriber__image[data-v-cb8085ba]{box-shadow:0 0 10px 1px #000}@media(max-width:480px){.subscriber[data-v-cb8085ba]{top:-2px;left:-23px}}.subscriber__stazh[data-v-cb8085ba]{position:absolute;right:0;bottom:0}.moder[data-v-cb8085ba]{left:-24px;top:40px;box-shadow:0 0 10px 1px #ff46f0}@media(max-width:480px){.moder[data-v-cb8085ba]{left:-13px;top:33px}}.nightBot[data-v-cb8085ba]{width:40px;height:40px;transition:.3s;left:-24px;background-color:hsla(0,0%,100%,.37);-webkit-animation:bot-data-v-cb8085ba .3s ease 1 forwards;animation:bot-data-v-cb8085ba .3s ease 1 forwards}@media(max-width:480px){.nightBot[data-v-cb8085ba]{left:-10px}}.gun[data-v-cb8085ba]{position:absolute;width:40px;height:40px;left:4px;bottom:30px}.gun__active[data-v-cb8085ba]{transform:rotate(-70deg);-webkit-animation:gun_shot-data-v-cb8085ba .1s ease 1 forwards;animation:gun_shot-data-v-cb8085ba .1s ease 1 forwards}.gun__active-false[data-v-cb8085ba]{transform:rotate(-70deg);-webkit-animation:gun_shoting-data-v-cb8085ba .2s ease 1 forwards;animation:gun_shoting-data-v-cb8085ba .2s ease 1 forwards}@media(max-width:480px){.gun[data-v-cb8085ba]{width:30px;height:30px;left:4px;bottom:5px;transform:rotate(-45deg)}}.active__sub[data-v-cb8085ba]{border:2px solid #ff7878;box-shadow:0 0 10px 1px #ff7878;background-color:#fff}.active__mod[data-v-cb8085ba]{border:2px solid #ff46f0;box-shadow:0 0 10px 1px #ff46f0;background-color:#fff}@-webkit-keyframes bot-data-v-cb8085ba{0%{width:50px;height:50px}to{width:30px;height:30px}}@keyframes bot-data-v-cb8085ba{0%{width:50px;height:50px}to{width:30px;height:30px}}@-webkit-keyframes appearance-item-data-v-cb8085ba{0%{transform:scaleX(0);background-color:#331414}to{transform:scaleX(1);background-color:rgba(0,0,0,.74)}}@keyframes appearance-item-data-v-cb8085ba{0%{transform:scaleX(0);background-color:#331414}to{transform:scaleX(1);background-color:rgba(0,0,0,.74)}}@-webkit-keyframes gun_shot-data-v-cb8085ba{0%{transform:rotate(0deg)}50%{transform:rotate(-70deg)}to{transform:rotate(0deg)}}@keyframes gun_shot-data-v-cb8085ba{0%{transform:rotate(0deg)}50%{transform:rotate(-70deg)}to{transform:rotate(0deg)}}@-webkit-keyframes gun_shoting-data-v-cb8085ba{0%{transform:rotate(0deg)}50%{transform:rotate(-71deg)}to{transform:rotate(1deg)}}@keyframes gun_shoting-data-v-cb8085ba{0%{transform:rotate(0deg)}50%{transform:rotate(-71deg)}to{transform:rotate(1deg)}}@-webkit-keyframes first-animate-data-v-cb8085ba{0%{transform:translate(0)}25%{transform:translate(20%)}50%{transform:translate(30%,20%)}75%{transform:translate(-10%,20%)}to{transform:translate(0)}}@keyframes first-animate-data-v-cb8085ba{0%{transform:translate(0)}25%{transform:translate(20%)}50%{transform:translate(30%,20%)}75%{transform:translate(-10%,20%)}to{transform:translate(0)}}@-webkit-keyframes second-animate-data-v-cb8085ba{0%{transform:translate(0)}25%{transform:translateY(10%)}50%{transform:translate(20%,20%)}75%{transform:translate(10%,-20%)}to{transform:translate(0)}}@keyframes second-animate-data-v-cb8085ba{0%{transform:translate(0)}25%{transform:translateY(10%)}50%{transform:translate(20%,20%)}75%{transform:translate(10%,-20%)}to{transform:translate(0)}}@-webkit-keyframes threee-animate-data-v-cb8085ba{0%{transform:translate(0)}25%{transform:translate(20%,-10%)}50%{transform:translate(5%,-20%)}75%{transform:translate(30%,10%)}to{transform:translate(0)}}@keyframes threee-animate-data-v-cb8085ba{0%{transform:translate(0)}25%{transform:translate(20%,-10%)}50%{transform:translate(5%,-20%)}75%{transform:translate(30%,10%)}to{transform:translate(0)}}.link-for-site[data-v-cb8085ba]{color:#fff;width:100%;font-family:\"MellaNissa\";z-index:99;position:absolute;text-align:center;bottom:5px;text-shadow:0 0 4px #000;font-size:22px}.link-for-site span[data-v-cb8085ba]{font-family:Arial,Helvetica,sans-serif}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/chat/v-kitty-chat.vue?vue&type=template&id=cb8085ba&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"chat"},[_vm._ssrNode("<div class=\"chat__content\" data-v-cb8085ba><img"+(_vm._ssrAttr("src",__webpack_require__(35)))+" alt class=\"cloud cloud--1 cloud__big\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(35)))+" alt class=\"cloud cloud--2 cloud__big\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(35)))+" alt class=\"cloud cloud--3 cloud__big\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(35)))+" alt class=\"cloud cloud--4 cloud__big\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(35)))+" alt class=\"cloud cloud--5 cloud__big\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(35)))+" alt class=\"cloud cloud--6 cloud__big\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(35)))+" alt class=\"cloud cloud--7 cloud__big\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(35)))+" alt class=\"cloud cloud--8 cloud__big\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(35)))+" alt class=\"cloud cloud--9 cloud__big\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(35)))+" alt class=\"cloud cloud--10 cloud__big\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(36)))+" alt class=\"cloud cloud--11 cloud__smoll\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(36)))+" alt class=\"cloud cloud--12 cloud__smoll\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(36)))+" alt class=\"cloud cloud--13 cloud__smoll\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(36)))+" alt class=\"cloud cloud--14 cloud__smoll\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(36)))+" alt class=\"cloud cloud--15 cloud__smoll\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(36)))+" alt class=\"cloud cloud--16 cloud__smoll\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(36)))+" alt class=\"cloud cloud--17 cloud__smoll\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(36)))+" alt class=\"cloud cloud--18 cloud__smoll\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(36)))+" alt class=\"cloud cloud--19 cloud__smoll\" data-v-cb8085ba> <img"+(_vm._ssrAttr("src",__webpack_require__(36)))+" alt class=\"cloud cloud--20 cloud__smoll\" data-v-cb8085ba> "+(_vm._ssrList((_vm.chat),function(komment,index){return ("<div class=\"chat__item\" data-v-cb8085ba><div"+(_vm._ssrClass("name",{'active__sub': komment.subscriber, 'active__mod' : komment.mod}))+(_vm._ssrStyle(null,("color: " + (komment.color)), null))+" data-v-cb8085ba><div class=\"subscriber\" data-v-cb8085ba>"+((komment.subscriber == true )?("<img"+(_vm._ssrAttr("src",__webpack_require__(37)))+" alt class=\"elit subscriber__image\" data-v-cb8085ba>"):"<!---->")+" "+((komment['badge-info.subscriber'])?("<span class=\"subscriber__stazh\" data-v-cb8085ba>"+_vm._ssrEscape(_vm._s(komment['badge-info.subscriber']))+"</span>"):"<!---->")+"</div> "+((komment.mod == true &&  komment['display-name'] != 'Nightbot' &&  komment['display-name'] != 'StreamElements' &&  komment['display-name'] != 'Moobot' )?("<img"+(_vm._ssrAttr("src",__webpack_require__(38)))+" alt class=\"elit moder\" data-v-cb8085ba>"):"<!---->")+" "+((komment['display-name'] === 'Nightbot' || komment['display-name'] === 'StreamElements' || komment['display-name'] === 'Moobot' )?("<img"+(_vm._ssrAttr("src",__webpack_require__(39)))+" alt class=\"elit nightBot\" data-v-cb8085ba>"):"<!---->")+_vm._ssrEscape("\n                    "+_vm._s(komment['display-name']))+"</div> <div"+(_vm._ssrClass("message",{'active__sub': komment.subscriber  , 'active__mod' : komment.mod}))+" data-v-cb8085ba>"+(_vm._s(_vm.emotionalKomment(komment)))+"</div></div>")}))+"</div> <img"+(_vm._ssrAttr("src",__webpack_require__(40)))+" alt"+(_vm._ssrClass("gun",{'gun__active': _vm.gun_active === true, 'gun__active-false': _vm.gun_active === false,}))+" data-v-cb8085ba> <p class=\"link-for-site\" data-v-cb8085ba>Rostomyan - inc.com</p>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/chat/v-kitty-chat.vue?vue&type=template&id=cb8085ba&scoped=true&

// EXTERNAL MODULE: external "tmi.js"
var external_tmi_js_ = __webpack_require__(34);
var external_tmi_js_default = /*#__PURE__*/__webpack_require__.n(external_tmi_js_);

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/chat/v-kitty-chat.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var v_kitty_chatvue_type_script_lang_js_ = ({
  props: {
    nick: {
      type: String,

      default() {
        return '';
      }

    }
  },
  name: 'v-chat',

  data() {
    return {
      gun_active: false
    };
  },

  methods: {
    emotionalKomment(komment) {
      if (!komment.emotes) {
        return komment.message;
      } else {
        const stringReplacements = [];
        Object.entries(komment.emotes).forEach(([id, positions]) => {
          const position = positions[0];
          const [start, end] = position.split("-");
          const stringToReplace = komment.message.substring(parseInt(start, 10), parseInt(end, 10) + 1);
          stringReplacements.push({
            stringToReplace: stringToReplace,
            replacement: ` <img style="width: 40px;" src="https://static-cdn.jtvnw.net/emoticons/v1/${id}/3.0"> `
          });
        });
        const messageHTML = stringReplacements.reduce((acc, {
          stringToReplace,
          replacement
        }) => {
          return acc.split(stringToReplace).join(replacement);
        }, komment.message);
        return messageHTML;
      }
    }

  },
  computed: {
    chat() {
      return this.$store.getters['chat/CHAT'];
    }

  },

  mounted() {
    const client = new external_tmi_js_default.a.Client({
      connection: {
        reconnect: true
      },
      channels: [this.nick]
    });
    client.connect();
    client.on('message', (channel, tags, message, self) => {
      this.gun_active = !this.gun_active;
      let new_koment = tags;

      if (tags['display-name'] == "dragnisimus") {
        tags.subscriber = true;
        tags.mod = true;
      }

      if (tags.subscriber == true) {
        console.log(tags);
      }

      tags.message = message;
      this.$store.commit('chat/SET_CHAT_IN_STATE', new_koment, message);
    });
  }

});
// CONCATENATED MODULE: ./components/chat/v-kitty-chat.vue?vue&type=script&lang=js&
 /* harmony default export */ var chat_v_kitty_chatvue_type_script_lang_js_ = (v_kitty_chatvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(2);

// CONCATENATED MODULE: ./components/chat/v-kitty-chat.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(56)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  chat_v_kitty_chatvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "cb8085ba",
  "2b204fe6"
  
)

/* harmony default export */ var v_kitty_chat = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=chat-v-kitty-chat.js.map